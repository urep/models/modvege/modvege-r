lintr::lint(filename = "./R/ModVege.R",
            linters = lintr::linters_with_defaults(
              line_length_linter = lintr::line_length_linter(115L),
              object_name_linter = lintr::object_name_linter(styles = c("CamelCase",
                                                                       "camelCase",
                                                                       "symbols")),
             object_length_linter = lintr::object_length_linter(length = 50L),
             cyclocomp_linter = NULL
           ))
