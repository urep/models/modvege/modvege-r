######### OPEN DATASET ##########

##### Importation ####
fileMeteo = "./DATA/DATACLIMATIK.csv"
DATA <- read.csv(file = fileMeteo, header = TRUE, sep = ";")
# Data formating
DATA$Time <- seq(1,length(DATA$RR),1)
DATA$PARi <- DATA$RG *0.48/100
DATA$jour <- rep(seq(1,365,1),8) # number of days

#### Scrambling data ####
# Temperature  : add between -20 and 20% of rain
set.seed(56732)
pourcentVector <- round(runif(length(DATA$Time),-20,20))
DATA$TMC <- DATA$TMC + DATA$TMC*(pourcentVector/100)
# PARi
set.seed(56732)
pourcentVector <- round(runif(length(DATA$Time),-20,20))
DATA$PARi <- DATA$PARi + DATA$PARi*(pourcentVector/100)
#RR : inter-change days of rain and add between -20 and 20% of rain
VEC <- DATA$RR 
i = 1
while(i < length(DATA$RR)){ # interchange loop
  # select 
 Probability <- sample(c(0,1), 1, replace=TRUE, prob=c(0.3,0.7)) # redemander à Raphaël !
 Last_day <- i%%365 
 
 if(Last_day == 0){i = i+1} # We don't exchange between years
 else if (Probability == 1){  # We change withe day i + 1
  tp <- DATA$RR[i]
  DATA$RR[i]= DATA$RR[i+1] 
  DATA$RR[i+1] = tp 
  i = i+2}
 else{i = i+1} # We don't exchange 
}
pourcent <- round(runif(length(DATA$Time),-20,20)) # Adding
DATA$RR <- DATA$RR + DATA$RR*(pourcent/100)
# ETPP : calculate from linear model with RG et temperature
DATA$ETPP <- (-0.1128+8.798)*(10^-4)*(DATA$PARi/0.48*100) +
 2.867*(10^-2)*DATA$TMC + 
 4.920*(10^-5)*(DATA$PARi/0.48*100)*DATA$TMC
for(i in 1:length(DATA$Time)){if (DATA$ETPP[i]<0){DATA$ETPP[i]<- 0}} # remove 0
# Take off RG
DATA <- DATA[,-4]

##### Exportation ####
write.csv(x = DATA, file = "./DATA/DATAPublic.csv")

##### Verification####
 # import DATA
fileMeteo = "./DATA/DATACLIMATIK.csv"
DATA_CLIMATIK <- read.csv(file = fileMeteo, header = TRUE, sep = ";")
DATA_CLIMATIK$Time <- seq(1,length(DATA_CLIMATIK$RR),1)
DATA_CLIMATIK$PARi <- DATA_CLIMATIK$RG *0.48/100
DATA_CLIMATIK$jour <- rep(seq(1,365,1),8) # number of days
 # temperature
par(mfrow=c(2,1))
c(max(DATA$TMC),min(DATA$TMC))
c(max(DATA_CLIMATIK$TMC),min(DATA_CLIMATIK$TMC))
plot(DATA_CLIMATIK$TMC,type = 'l')
plot(DATA$TMC,type = 'l')
 # PARi
c(max(DATA$PARi),min(DATA$PARi))
c(max(DATA_CLIMATIK$PARi),min(DATA_CLIMATIK$PARi))
plot(DATA$PARi,type = 'l')
plot(DATA_CLIMATIK$PARi,type = 'l')
 # Precipitation RR
c(max(DATA$RR),min(DATA$RR))
c(max(DATA_CLIMATIK$RR),min(DATA_CLIMATIK$RR))
plot(DATA$Time, DATA$RR,type = 'l')
plot(DATA_CLIMATIK$Time,DATA_CLIMATIK$RR,type = 'l')
 # ETPP
c(max(DATA$ETPP),min(DATA$ETPP))
c(max(DATA_CLIMATIK$ETPP),min(DATA_CLIMATIK$ETPP))
plot(DATA$ETPP,type = 'l')
plot(DATA_CLIMATIK$ETPP,type = 'l')

